package todo

import (
	"context"
	"errors"
	"todo/internal/models"
	"todo/storage"
)

type TodoStorage interface {
	Get(ctx context.Context) ([]models.Todo, error)
}

type TodoService struct {
	storage TodoStorage
}

func (t *TodoService) Get() ([]models.Todo, error) {
	data, err := t.storage.Get()
	if errors.Is(err, storage.ErrNoElements) {
		return []models.Todo{}, nil
	}
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (t *TodoService) Create(todo models.Todo) error {
	return t.storage.Create(&todo)
}

func New(storage TodoStorage) *TodoService {
	return &TodoService{}
}
