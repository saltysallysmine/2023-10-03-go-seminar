package app

import (
	_ "embed"

	"todo/internal/config"
	"todo/storage/sqlite"
)

// go:embed init.sql
var migration string

type App struct {
	cfg     *config.Config
	storage *sqlite.Storage
}

func NewApp(cfg *config.Config) *App {
	storage, err := sqlite.StorageNew(cfg.DB.Path)
	if err != nil {
		panic("failed with open db")
	}
	err = storage.Migrate(migration)
	if err != nil {
		panic(err)
	}
	todoService := services.New(storage)
	return &App{cfg: cfg}
}

func (*App) Run() {
}

func (*App) Stop() {
}
