create table if not exist todo(
  id integer PRIMARY KEY AUTOINCREMENT 
  task TEXT, 
  deleted_at TIMESTAMP NULL
)
