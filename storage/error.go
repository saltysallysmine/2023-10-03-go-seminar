package storage

var (
	ErrNoElements = &StorageError{Message: "no elements"}
)

type StorageError struct { // чтобы стало ошибкой нужно реализовать интерфейс
	Message string
}

func (se *StorageError) Error() string { // реализуем
	return se.Message
}
