package sqlite

import (
	"context"
	"database/sql" // описаны только интерфейсы
	"errors"
	"fmt"

	_ "github.com/go-sqlite/sqlite3" // а это драйвер

	"todo/internal/models"
	"todo/storage"
)

type Storage struct {
	db *sql.DB
}

func (s *Storage) Migrate(script ...string) error {
	tx, _ := s.db.Begin()
	defer tx.Rollback() // зачем?
	for _, v := range script {
		_, err := tx.Exec(v)
		if err != nil {
			_ = tx.Rollback()
			return err
		}
	}
	return tx.Commit()
}

func (s *Storage) Get(ctx context.Context) ([]models.Todo, error) {
	data, err := s.db.QueryContext(ctx, `select id, task, deleted_at
  from todo where deleted_at is null`) // лучше использовать его, вместо Query
	if errors.Is(err, sql.ErrNoRows) {
		return nil, fmt.Errorf("storage.Get(); todo not found %w", storage.ErrNoElements)
	}
	if err != nil {
		return nil, err
	}
	todos := make([]models.Todo, 0)
	var todo models.Todo
	for data.Next() { // удобно
		data.Scan(&todo.ID, &todo.Task, &todo.DeletedAt)
		todos = append(todos, todo)
	}
	return todos, err
}

func StorageNew(filePath string) (*Storage, error) {
	db, err := sql.Open("sqlite", filePath)
	if err != nil {
		return nil, err
	}
	return &Storage{db: db}, err
}
