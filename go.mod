module todo

go 1.21.1

require gopkg.in/yaml.v3 v3.0.1

require (
	github.com/go-sqlite/sqlite3 v0.0.0-20180313105335-53dd8e640ee7 // indirect
	github.com/gonuts/binary v0.2.0 // indirect
)
