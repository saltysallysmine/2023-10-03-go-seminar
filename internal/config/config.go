package config

import (
	"os"

	"gopkg.in/yaml.v3"
)

type Config struct {
	Http Http `yaml:"http"`
	DB   DB   `yaml:"db"`
}

type Http struct {
	Port int `yaml:"port"`
}

type DB struct {
	Path string `yaml:"path"`
}

func Parse(filePath string) (*Config, error) {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}
	var cfg Config
	yaml.Unmarshal(data, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, err
}
