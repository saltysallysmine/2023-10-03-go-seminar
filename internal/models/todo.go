package models

import "time"

type Todo struct {
	ID        int        `json:"ID"`
	Task      string     `json:"task"`
	DeletedAt *time.Time `json:"deletedAt"` // чтобы в программе он был not null, но в БД мог таким быть
}
