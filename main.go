package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"syscall"
	"todo/app"
	"todo/internal/config"
)

func main() {
	var filePath string
	flag.StringVar(&filePath, "c", "internal/.config.yaml", "set config path")
	flag.Parse()

	cfg, err := config.Parse(filePath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	app := app.NewApp(cfg)
	app.Run()

	ctx, stop := signal.notifyContext(context.Background(), syscall.SIGINT,
		syscall.SIGTERM)
	defer stop()
	defer app.Stop()
	defer func() {
		if v := recover(); v != nil {
			fmt.Println(v)
			os.Exit(1)
		}
	}()
	<-ctx.Done()
}
